<?php
include_once ('../../vendor/autoload.php');
use App\Library\Library;
$obj= new Library();
$allData=$obj->index();

$trs="";
$sl=0;
foreach($allData as $data):
    $sl++;
    $trs.="<tr>";
    $trs.="<td> $sl</td>";
    $trs.="<td> $data->name</td>";
    $trs.="<td> $data->author</td>";
    $trs.="<td> $data->edition</td>";
    $trs.="<td> $data->category</td>";
    $trs.="<td> $data->location</td>";
    $trs.="<td> $data->amount</td>";
    $trs.="</tr>";
endforeach;
//print_r($trs);
$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Book Name</th>
                    <th>Author Name</th>
                    <th>Edition</th>
                    <th>Category</th>
                    <th>Location</th>
                    <th>Amount</th>
                </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>




BITM;


// Require composer autoload
require_once ('../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
//$mpdf->Output();
$mpdf->Output('Booklist.pdf','D');

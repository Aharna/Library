<?php

session_start();

include_once('../../vendor/autoload.php');
use App\Library\Library;
use App\Utility\Utility;
use App\Message\Message;

$obj = new Library();
$book_id = $obj->prepare($_POST)->getBookId();
$_POST['book_id'] =$book_id;
$is_issued = $obj->prepare($_POST)->is_issued();
if($is_issued){

    $obj->prepare($_POST)->increaseBookAmount();
    $due_date = $obj->prepare($_POST)->returnBook();


//$date1=date_create(date("Y-m-d"));
//$date2=date_create($due_date);
//$diff=date_diff($date1,$date2);
//$delay = $diff->format("%R%a");


    $to_time = strtotime($due_date);
    $from_time = strtotime(date("Y-m-d"));
    $delay = round(($to_time - $from_time)/(60*60*24),0);

    if($delay>=0){
        $_SESSION['fine_amount']= "0.00";
    }
    else{
        $amount = abs(5.00*$delay);
        $_SESSION['fine_amount']= $amount;
    }
}

else{
    Message::message("Wrong Entry! Please insert correct informations");
}

Utility::redirect('return_book.php');
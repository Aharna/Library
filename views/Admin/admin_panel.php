<?php

session_start();

include_once('../../vendor/autoload.php');

use App\Admin\Auth;
use App\Library\Library;
use App\Utility\Utility;
use App\Message\Message;

$auth = new Auth();
$logged_in = FALSE;
if (!$auth->logged_in())
    Utility::redirect("admin_login.php");

if (array_key_exists("itemPerPage", $_SESSION)) {
    if (array_key_exists("itemPerPage", $_GET))
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
} else
    $_SESSION['itemPerPage'] = 5;

$itemPerPage = $_SESSION['itemPerPage'];

$obj = new Library();
$book = $obj->panel();
$totalItem = $obj->count();
//echo $book;
//Utility::dd($book);
$totalPage = ceil($totalItem / $itemPerPage);

if (array_key_exists("pageNumber", $_GET))
    $pageNumber = $_GET['pageNumber'];
else
    $pageNumber = 1;

$pagination = "";
for ($count = 1; $count <= $totalPage; $count++) {
    $class = ($pageNumber == $count) ? "active" : "";
    $pagination .= "<li class='$class'><a href='index.php?pageNumber=$count'>$count</a></li>";
}


$pageStartFrom = $itemPerPage * ($pageNumber - 1);

//$allInfo = $obj->paginator($pageStartFrom,$itemPerPage);


if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) {
    $allInfo = $obj->paginator($pageStartFrom, $itemPerPage);
}
if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET') && isset($_GET['category'])) {
    $obj->prepare($_GET);
    $allInfo = $obj->index();
}
if ((strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) && isset($_GET['search'])) {
    $obj->prepare($_GET);
    $allInfo = $obj->index();
}

//$availableTitle=$obj->getAllTitle();
//$comma_separated= '"'.implode('","',$availableTitle).'"';
//
//$availableDescription=$obj->getAllDescription();
//$comma_separated2= '"'.implode('","',$availableDescription).'"';
////Utility::dd($comma_separated);

?>


<!DOCTYPE html>
<html>
<head>
    <title>Admin Panel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../../Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="../../Resources/bootstrap/css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                <!-- Logo -->
                <div class="logo">
                    <h1><a href="index.php">Library Management System</a></h1>
                </div>
            </div>
            <div class="col-md-4">
                <div style="margin-top: 8px" class="row">
                    <div class="col-lg-12">
                        <div>
                            <form style="float: right" class="form-inline" action="index.php" method="get">

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--            <div class="col-md-5">-->
            <!--                <div class="row">-->
            <!--                    <div class="col-lg-12">-->
            <!--                        <div class="input-group form">-->
            <!--                            <input type="text" class="form-control" placeholder="Search...">-->
            <!--	                       <span class="input-group-btn">-->
            <!--	                         <button class="btn btn-primary" type="button">Search</button>-->
            <!--	                       </span>-->
            <!--                        </div>-->
            <!--                    </div>-->
            <!--                </div>-->
            <!--            </div>-->
            <div class="col-md-2">
                <div class="navbar navbar-inverse" role="banner">
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b
                                        class="caret"></b></a>
                                <ul class="dropdown-menu animated fadeInUp">
                                    <li><a href="admin_logout.php">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li class="current"><a href="index.php"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
                    <li><a href="forms.php"><i class="glyphicon glyphicon-tasks"></i> Add Book</a></li>
                    <li><a href="requested_books.php"><i class="glyphicon glyphicon-book"></i> Requested Book</a></li>
                    <li><a href="issued_book.php"><i class="glyphicon glyphicon-book"></i> Issued Book</a></li>
                    <li><a href="return_book.php"><i class="glyphicon glyphicon-backward"></i> Return Book</a></li>
                    <li><a href="admin_panel.php"><i class="glyphicon glyphicon-backward"></i> Admin Panel</a></li>
                    <li class="submenu">
                        <a href="#">
                            <i class="glyphicon glyphicon-list"></i> Pages
                            <span class="caret pull-right"></span>
                        </a>
                        <!-- Sub menu -->
                        <ul>
                            <li><a href="../../index.php">Normal Index</a></li>
                            <li><a href="../index.php">User Index</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-10">

            <?php if(array_key_exists('message',$_SESSION) && !empty($_SESSION['message'])) : ?>
                <div class="alert-info" id="message">
                    <h4><center><?php echo Message::message() ?></center></h4>
                </div>
            <?php endif; ?>

            <div align="right">
                <?php if (strtoupper($_SESSION['admin_name']) == "FAHAD") : ?>
                    <a href="admin_register.php#toregister" class="btn btn-primary" role="button">Create Another
                        Admin</a>
                <?php endif; ?>

            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Serial</th>
                        <th>Admin Name</th>
                        <?php if (strtoupper($_SESSION['admin_name']) == "FAHAD") : ?>
                            <th>Action</th>
                        <?php endif; ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $sl = 0;
                    foreach ($book as $info) :

                        $sl++; ?>

                        <tr>
                            <td><?php echo $sl + $pageStartFrom ?></td>

                            <td> <?php echo $info->username ?> </td>
                            <?php if (strtoupper($_SESSION['admin_name']) == "FAHAD") : ?>
                            <td><a href="delete_admin.php?username=<?php echo $info->username ?>"id="delete" onclick=" return ConfirmDelete()" class="btn btn-danger"
                                   role="button">Delete</a>
                                <?php endif; ?>
                        </tr>
                    <?php endforeach; ?>


                    </tbody>
                </table>

            </div>
            <?php if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) { ?>
                <!--            <ul class="pagination">-->
                <!--                --><?php //if($pageNumber>1):?><!-- <li><a href='index.php?pageNumber=--><?php //echo $pageNumber-1 ?><!--'> Prev </a></li> --><?php //endif;?>
                <!--                --><?php //echo $pagination; ?>
                <!--                --><?php //if($pageNumber<$totalPage):?><!-- <li><a href='index.php?pageNumber=--><?php //echo $pageNumber+1 ?><!--'> Next </a></li> --><?php //endif;?>
                <!--            </ul>-->
            <?php } ?>

        </div>


    </div>
</div>
</div>

<footer>
    <div class="container">

        <div class="copy text-center">
            Copyright 2016 Core i5
        </div>

    </div>
</footer>
<script>
    $('#message').show().delay(1200).fadeOut();


    //        $(document).ready(function(){
    //            $("#delete").click(function(){
    //                if (!confirm("Do you want to delete")){
    //                    return false;
    //                }
    //            });
    //        });
    function ConfirmDelete() {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }
    $('#multiple_delete').on('click', function () {
        document.forms[0].action = "deleteMultiple.php";
        $('#multiple').submit();
    });


</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../Resources/bootstrap/js/bootstrap.min.js"></script>
<script src="../../Resources/bootstrap/js/custom.js"></script>
</body>
</html>
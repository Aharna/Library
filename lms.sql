-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2016 at 08:50 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lms`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('Aharna', '81dc9bdb52d04dc20036dbd8313ed055'),
('Fahad', '81dc9bdb52d04dc20036dbd8313ed055'),
('Jannat', '81dc9bdb52d04dc20036dbd8313ed055'),
('Ranjoy', '81dc9bdb52d04dc20036dbd8313ed055'),
('Suvasish', '81dc9bdb52d04dc20036dbd8313ed055');

-- --------------------------------------------------------

--
-- Table structure for table `booklist`
--

CREATE TABLE `booklist` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `edition` varchar(20) NOT NULL,
  `category` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `location` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `cover` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booklist`
--

INSERT INTO `booklist` (`id`, `name`, `author`, `edition`, `category`, `type`, `location`, `amount`, `cover`) VALUES
(14, 'Programming in C ', 'Stephen Kochan', '3rd', 'Programming', '', 'Shelf 1', 30, '1470204038download.png'),
(15, 'How to Program', 'Paul Deitel & Harvey M. Deite', '6th', 'Programming', '', 'Shelf 1', 20, '1470205336download3.jpg'),
(16, 'C Primer Plus - ', 'Stephen Prata', '6th', 'Programming', '', 'Shelf 1', 24, '1470205897download4.jpg'),
(17, 'C - Traps and Pitfalls ', 'Andrew R. Koenig (Bell Labs)', '2nd', 'Programming', '', 'Shelf 1', 30, '1470206101download5.jpg'),
(18, 'Programming: Principles and Practice Using C++ ', 'Bjarne Stroustrup', '2nd', 'Programming', '', 'Shelf 1', 25, '1470206631download6.jpg'),
(19, 'C++ Primer * ', 'Stanley Lippman, JosÃ©e Lajoie, and Barbara E. Moo', '5th', 'Programming', '', 'Shelf 1', 20, '1470206745download7.jpg'),
(20, 'Exceptional C++ Style ', 'Herb Sutter', '2nd', 'Programming', '', 'Shelf 1', 26, '1470206899download8.jpg'),
(21, 'Java in a Nutshell', 'Benjamin,J.Evans and David Flangan', '6th Edition', 'Programming', '', 'Shelf 1', 30, '1470207474download9.jpg'),
(22, 'Hooked on Java: Creating hot Web sites with Java applets', 'Van Hoff, Arthur', '1st', 'Programming', '', 'Shelf 1', 30, '147020779341jzgZmIlAL.jpg'),
(23, 'The Truth About HTML5', 'Luke Stevens', '1st', 'Programming', '', 'Shelf 1', 0, '1470208027file.jpeg'),
(24, ' The Essential Guide to CSS and HTML Web Design', 'Craig Grannell', '1st', 'Programming', '', 'Shelf 1', 20, '1470208200file (1).jpeg'),
(25, ' Smashing CSS', 'Eric Meyer', '1st', 'Programming', '', 'Shelf 1', 15, '1470208367file (2).jpeg'),
(26, 'Core HTML5 Canvas', 'David Geary', '1st', 'Programming', '', 'Shelf 1', 27, '1470208533file (3).jpeg'),
(27, ' Styling with CSS', 'Charles Wyke Smith', '3rd', 'Programming', '', 'Shelf 1', 17, '1470208697file (4).jpeg'),
(28, 'PHP & MySQL: Novice to Ninja', 'Kevin Yank', '5th', 'Programming', '', 'Shelf 1', 21, '1470208918download10.jpg'),
(29, 'PHP for the Web: Visual QuickStart Guide ', 'Larry Ullman', '$th', 'Programming', '', 'Shelf 1', 15, '1470209033download11.jpg'),
(30, 'Learning PHP, MySQL & JavaScript: With jQuery, CSS & HTML5 ', 'Robin Nixon', '1st', 'Programming', '', 'Shelf 1', 17, '1470209155download12.jpg'),
(31, 'PHP Objects, Patterns, and Practice ', 'Matt Zandstra', '4th', 'Programming', '', 'Shelf 1', 19, '1470209264download13.jpg'),
(32, 'Advanced PHP Programming ', 'George Schlossnagle', '2nd', 'Programming', '', 'Shelf 1', 22, '1470209426download14.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `borrow`
--

CREATE TABLE `borrow` (
  `borrow_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `borrow_date` date NOT NULL,
  `due_date` date NOT NULL,
  `return_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `requested_books`
--

CREATE TABLE `requested_books` (
  `request_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `dept` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `dept`, `password`) VALUES
(12205010, 'Suvasish', 'Computer Science', '81dc9bdb52d04dc20036dbd8313ed055'),
(12205011, 'Aharna', 'Computer Science', '81dc9bdb52d04dc20036dbd8313ed055'),
(12205012, 'Jannat', 'Computer Science', '81dc9bdb52d04dc20036dbd8313ed055'),
(12205016, 'Ranjoy', 'Computer Science', '81dc9bdb52d04dc20036dbd8313ed055'),
(12205051, 'Fahad', 'Computer Science', '81dc9bdb52d04dc20036dbd8313ed055');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `booklist`
--
ALTER TABLE `booklist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `borrow`
--
ALTER TABLE `borrow`
  ADD PRIMARY KEY (`borrow_id`);

--
-- Indexes for table `requested_books`
--
ALTER TABLE `requested_books`
  ADD PRIMARY KEY (`request_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booklist`
--
ALTER TABLE `booklist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `borrow`
--
ALTER TABLE `borrow`
  MODIFY `borrow_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `requested_books`
--
ALTER TABLE `requested_books`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
